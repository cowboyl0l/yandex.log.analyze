﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Yandex.Log.Analize.Common;

namespace Yandex.Log.Analize
{
	class Program
	{
		static void Main(string[] args)
		{
			var listResults = new List<LogItem>();
			var skipRows = 1;
			using (var streamReader = new StreamReader("Log"))
			{
				var currentRowIndex = 0;
				while (streamReader.Peek() >= 0)
				{
					if (currentRowIndex < skipRows)
					{
						currentRowIndex++;
						streamReader.ReadLine();
						continue;
					}
					var line = streamReader.ReadLine();
					var partsOfLine = line.Split('	');
					var hrefs = partsOfLine.Last().Split(';');
					listResults.Add(new LogItem()
					{
						DateTime = UnixTimeStampToDateTime(partsOfLine[0]),
						Device = (DeviceEnum)Enum.Parse(typeof(DeviceEnum), partsOfLine[2]),
						Numdoc = long.Parse(partsOfLine[3]),
						Regiom = int.Parse(partsOfLine[4]),
						Request = (partsOfLine[5].Replace("https://yandex.ru/search/?text=", String.Empty)),
						Links = hrefs
					});
				}

			}

			var adultLogs = listResults.Where(x => AnalyzeHelper.adultKeys.Contains(x.Request)).ToArray();
			var adultUrls = adultLogs.SelectMany(x => x.Links).Distinct().ToArray();
			var pornRequests = adultLogs.Select(x => x.Request).Distinct();

			var tvlogsWithoutFilter = listResults.Where(x => AnalyzeHelper.filmsKeys.Any(y=> x.Request.Contains(y)));
			var tvlogsWithFilter = listResults.Where(x => AnalyzeHelper.filmsKeys.Any(y => x.Request.Contains(y) && !x.Links.Any(yy=> adultUrls.Contains(yy))) && !AnalyzeHelper.adultKeys.Contains(x.Request)).ToList();
		}

		private static DateTime UnixTimeStampToDateTime(string unixTimeStamp)
		{
			// Unix timestamp is seconds past epoch
			System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
			dtDateTime = dtDateTime.AddSeconds(double.Parse(unixTimeStamp)).ToLocalTime();
			return dtDateTime;
		}
	}
}
