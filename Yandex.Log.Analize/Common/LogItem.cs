﻿using System;
using System.Collections.Generic;

namespace Yandex.Log.Analize.Common
{
	public class LogItem
	{
		public DateTime DateTime { get; set; }
		public DeviceEnum Device { get; set; }
		public long Numdoc { get; set; }
		public int Regiom { get; set; }
		public string Request { get; set; }
		public string[] Links { get; set; }
	}
}